<?php

use Codeception\Util\HttpCode;

class CreateEmployeeCest
{
    public function _before(ApiTester $I)
    {
        $I->sendPOST('/auth/login', ['email' => $I->email, 'password' => $I->password]);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseJsonMatchesJsonPath('$.access_token');
        $I->token = $I->grabDataFromResponseByJsonPath('$.access_token')[0];
    }

    // tests
    public function createEmployee(ApiTester $I)
    {
        $I->wantTo('Upload a CSV File to create Employees');
        $I->amBearerAuthenticated($I->token);
        $I->sendPOST('/employees/csv', [], ['employees' => 'tests/_data/employees.csv']);
        $I->seeResponseCodeIs(HttpCode::ACCEPTED);
    }

    public function checkCreatedEmployee(ApiTester $I)
    {
        $I->wantTo('Check if csv really updated employees');
        $I->amBearerAuthenticated($I->token);
        $I->sendGET('/employees');
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'name' => 'Sra. Micaela Sandoval Gil',
            'email' => 'misandoval@globo.com',
            'document' => '',
            'city' => 'Ferreira do Norte',
            'state' => 'Tocantins',
            'start_date' => '2019-03-18',
            'manager_id' => '41b6c1f7-1d2a-4050-a28d-4861a65d948f'
        ]);
    }

    public function checkSuccessfulMail(ApiTester $I)
    {
        $I->wantTo('Check if mail was sent (20+seconds)');
        //waiting for email reach mailbox
        sleep(20);
        $I->sendGET('https://tempr.email/en/rss/convenia-teste-1@discardmail.de:d264aada9efbebec5a779bc011888289');
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsXml();
        $I->seeXmlResponseMatchesXpath(
            "//channel/item/title[text()='" . getenv('APP_NAME') . ": Processamento de Colaboradores']"
        );
    }
}
