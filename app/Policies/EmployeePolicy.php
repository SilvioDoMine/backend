<?php

namespace App\Policies;

use App\Employee;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class EmployeePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can delete the employee.
     *
     * @param   \App\User  $user
     * @param   \App\Employee  $employee
     * @return  \Illuminate\Auth\Access\Response
     */
    public function delete(User $user, Employee $employee)
    {
        return $user->id === $employee->manager_id
                ? Response::allow()
                : Response::deny('Você não tem autorização para excluir este usuário.');
    }
}
