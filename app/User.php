<?php

namespace App;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, Uuid;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Indica o tipo da chave primária do modelo.
     * NOTA: Isso precisa estar string ou causará erro no MySQL, pois
     * estamos utilizando UUID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indica se o ID está auto incrementando ou não.
     * NOTA: Isso precisa estar falso ou causará erro no MySQL, pois
     * estamos utilizando UUID.
     * 
     * @var boolean
     */
    public $incrementing = false;

    /**
     * Retorna uma lista com todos os funcionários do usuário
     * 
     * @return  \Illuminate\Database\Eloquent\Collection
     */
    public function employees()
    {
        return $this->hasMany(Employee::class, 'manager_id');
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
