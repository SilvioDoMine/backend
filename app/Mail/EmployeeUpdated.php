<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmployeeUpdated extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * E-mail do usuário para qual nós estamos disparando.
     * 
     * @var string
     */
    public string $email;

    /**
     * Create a new message instance.
     *
     * @param   string  $email  E-mail do remetente
     * @return  void
     */
    public function __construct(string $email)
    {
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $systemName = config('app.name');
        $systemEmail = config('mail.from.address');

        return $this->subject("{$systemName} - Seus dados foram atualizados!")
                    ->from($systemEmail)
                    ->to($this->email)
                    ->view('mails.employee.updated');
    }
}
