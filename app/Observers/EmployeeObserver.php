<?php

namespace App\Observers;

use App\Employee;
use App\Jobs\sendEmail;
use App\Mail\EmployeeCreated;
use App\Mail\EmployeeUpdated;

class EmployeeObserver
{
    /**
     * Handle the employee "created" event.
     *
     * @param  \App\Employee  $employee
     * @return void
     */
    public function created(Employee $employee)
    {
        # Enviar e-mail: você foi incluído na plataforma.
        sendEmail::dispatch(new EmployeeCreated($employee->email));
    }

    /**
     * Handle the employee "updated" event.
     *
     * @param  \App\Employee  $employee
     * @return void
     */
    public function updated(Employee $employee)
    {
        # Enviar e-mail: seus dados foram atualizados.
        sendEmail::dispatch(new EmployeeUpdated($employee->email));
    }
}
