<?php

namespace App\Jobs;

use App\Employee;
use App\Services\CsvService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class EmployeesCsvProcessor implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Instância do CSV Service
     * 
     * @var CsvService
     */
    private CsvService  $Csv;

    /**
     * UUID do usuário que deu upload no CSV.
     * 
     * @var string
     */
    private string  $userUuid;
    
    /**
     * Nome do CSV (Tmp), direto do Request.
     * 
     * @var string
     */
    private string  $csvName;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $userUuid, string $csvName)
    {
        $this->userUuid = $userUuid;
        $this->csvName = $csvName;
        $this->Csv = new CsvService($csvName);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()    
    {
        # Pega todos os employees do CSV.
        $employees = $this->Csv->getRowsWithKeys();

        # Tenta armazená-los no banco de dados.
        $this->registerEmployees($employees);
    }

    /**
     * Recebe uma lista de funcionários diretamente do CSV
     * e tenta inserí-los no banco de dados.
     * 
     * @param   array   $employees
     * @return  void
     */
    private function registerEmployees(array $employees): void
    {
        foreach ($employees as $employee) {
            Employee::updateOrCreate([
                'email' => $employee['email'],
                'document' => $employee['document'],
                'manager_id' => $this->userUuid,
            ], $employee);
        }
    }
}
