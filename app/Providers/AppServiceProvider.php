<?php

namespace App\Providers;

use App\Employee;
use App\Observers\EmployeeObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        # Vamos registrar esse singleton para usar a instância em PT-BR
        # do Faker dentro das nossas seeds.
        $this->app->singleton(\Faker\Generator::class, function () {
            return \Faker\Factory::create('pt_BR');
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        # Vamos registrar um Observer na model de Employees.
        Employee::observe(EmployeeObserver::class);
    }
}
