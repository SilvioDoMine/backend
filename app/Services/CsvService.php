<?php

namespace App\Services;

class CsvService
{
    /**
     * Caminho do arquivo CSV.
     * 
     * @var string
     */
    private $path;

    /**
     * Todas as colunas do CSV, incluindo os headers.
     * 
     * @var array
     */
    private array $rows;

    /**
     * Cria uma nova instância do CsvService
     * 
     * @return  void
     */
    public function __construct($path)
    {
        $this->path = $path;
        $this->bootCsv();
    }

    /**
     * Inicializa o CSV, lê e o sanatiza.
     * 
     * @return  void
     */
    private function bootCsv(): void
    {
        $rows = file($this->path);
        $this->rows = $this->sanitizeAllRows($rows);
    }

    /**
     * Remove todos os fim de linhas (LF/CRLF) das Rows
     * do CSV.
     * 
     * @param   array   $rows
     * @return  array
     */
    private function sanitizeAllRows(array $rows): array
    {
        foreach ($rows as &$row) {
            $row = trim(preg_replace('/\s+/', ' ', $row));
        }

        return $rows;
    }

    /**
     * Retorna a lista de Headers do CSV atual.
     * 
     * @return  array
     */
    public function getHeaders(): array
    {
        return explode(',', $this->rows[0]);
    }

    /**
     * Retorna a lista de Rows do CSV atual.
     * 
     * @return  array
     */
    public function getRows(): array
    {
        $rows = [];

        $allRows = $this->rows;

        array_shift($allRows);

        foreach ($allRows as $row) {
            $rows[] = explode(',', $row);
        }

        return $rows;
    }

    /**
     * Retorna a lista de rows, com os headers como chave
     * de cada valor.
     * 
     * @return  array
     */
    public function getRowsWithKeys()
    {
        $headers = $this->getHeaders();
        $rows = $this->getRows();

        $newRows = [];

        # Para cada linha...
        foreach ($rows as $rowKey => $row) {
            # Vamos varrer cada elemento...
            foreach ($row as $itemKey => $item) {
                # E vamos adicionar dentro da array $newRows.
                $newRows[$rowKey][$headers[$itemKey]] = $item;
            }
        
        }

        return $newRows;
    }
}