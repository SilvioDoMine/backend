<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;
use Tymon\JWTAuth\Facades\JWTAuth;

class ApiValidate extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            # Tenta pegar o token do usuário autenticado.
            JWTAuth::parseToken()->authenticate();
        } catch (Exception $e) {
            # Caso ele não consiga, uma Exception será lançada. Qual erro nós encontraremos?
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException) {
                return response()->json(['message' => 'Token de autenticação inválido.'], 401);
            } else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException) {
                return response()->json(['message' => 'Token de autenticação expirado.'], 401);
            } else {
                return response()->json(['message' => 'Não foi encontrado um token de autenticação.'], 401);
            }
        }

        return $next($request);
    }
}
