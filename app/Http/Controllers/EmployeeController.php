<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Jobs\EmployeesCsvProcessor;
use App\Jobs\sendEmail;
use App\Mail\EmployeeCsvUploaded;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    /**
     * Lista todos os usuários do cliente.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $employees = auth()->user()->employees;

        return response()->json(['message' => 'Listagem de todos seus funcionários', 'data' => $employees]);
    }

    /**
     * Mostro um funcionário específico baseado no e-mail.
     *
     * @param  string  $uuid
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(string $uuid)
    {
        $employee = Employee::findByUuid($uuid);

        if ($employee) {
            return response()->json(['message' => "Listando usuário de id {$uuid}", 'data' => $employee]);
        }

        return response()->json(['message' => 'Não foi possível encontrar o usuário.'], 404);
    }

    /**
     * Método que recebe um CSV, sanatíza-o e dispara um job para processá-lo.
     * 
     * @param   \Illuminate\Http\Request
     * @return  \Illuminate\Http\JsonResponse
     */
    public function upload(Request $request)
    {
        $user = auth()->user();

        # Dispara um Job, passando o ID do usuário atual e o CSV.
        # Caso o job seja bem sucedido, vamos disparar um evento
        # em cadeia de disparar um e-mail.
        EmployeesCsvProcessor::withChain([
            new sendEmail(new EmployeeCsvUploaded($user->email))
        ])->dispatch($user->id, $request->employees);

        return response()->json(['mensagem' => 'Recebemos sua planilha! Estamos atualizando, enviaremos um e-mail quando estiver tudo pronto.'], 202);
    }

    /**
     * Método que deleta um colaborador do sistema.
     * 
     * @param   \App\Employee   $employee
     * @return  \Illuminate\Http\JsonResponse
     */
    public function destroy(Employee $employee)
    { 
        # Verifica se o usuário está autenticado, utilizando a Policy.
        $this->authorize('delete', $employee);
        
        $employee->delete();

        return response()->json(['message' => "O colaborador {$employee->id} de e-mail {$employee->email} foi excluído do sistema."], 204);
    }
}
