<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;

class AuthController extends Controller
{
    /**
     * Cria uma nova instância de AuthController.
     * 
     * @return  void
     */
    public function __construct()
    {
        # Para todos os métodos desse controlador (com exceção do login)
        # iremos adicionar a middleware apiValidate
        $this->middleware('apiValidate', ['except' => ['login']]);
    }

    /**
     * Recebe uma requisição para logar o usuário com JWT.
     * 
     * @param   \App\Http\Requests\LoginRequest
     * @return  \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request)
    {
        $credentials = $request->only(['email', 'password']);

        if (!$token = auth('api')->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    /**
     * Desloga o usuário que realizou a requisição. Desativa o seu token JWT.
     * 
     * @return  \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60
        ]);
    }
}
