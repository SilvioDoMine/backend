<?php

namespace App;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use Uuid;

    /**
     * Lista de atributos que podem receber atribuição em massa.
     * 
     * @var array
     */
    protected $guarded = [
        'created_at', 'updated_at', 'deleted_at',
    ];

    /**
     * Lista de atributos da model que devem ser "castados" para o
     * seu tipo nativo.
     * 
     * @var array
     */
    protected $casts = [
        'start_date' => 'date:Y-m-d',
    ];

    /**
     * Indica o tipo da chave primária do modelo.
     * NOTA: Isso precisa estar string ou causará erro no MySQL, pois
     * estamos utilizando UUID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Define a chave de busca no "Implicit Binding" do Laravel.
     * NOTA: No Laravel 7x^ isso pode ser setado dinamicamente na rota.
     *
     * @return  string
     */
    public function getRouteKeyName()
    {
        return 'document';
    }

    /**
     * Retorna o usuário gerente desse empregado.
     * 
     * @return  \App\User|null
     */
    public function manager()
    {
        return $this->belongsTo(User::class, 'manager_id');
    }

    /**
     * Procura um funcionário do usuário atual no banco de dados pelo uuid.
     * 
     * @param   string  $uuid
     * @return  \App\Employee|null
     */
    public static function findByUuid(string $uuid)
    {
        return self::where('manager_id', auth()->user()->id)->where('id', $uuid)->first();
    }
}
