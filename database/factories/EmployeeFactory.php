<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Employee;
use Faker\Generator as Faker;

$factory->define(Employee::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'document' => $faker->cpf,
        'city' => $faker->city,
        'state' => $faker->state,
        'start_date' => now()->format('Y-m-d'),
        'manager_id' => 'changed_by_the_seeder',
    ];
});
