<?php

use App\Employee;
use App\User;
use Illuminate\Database\Seeder;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        # Para cada usuário do sistema, vamos inserir 3 funcionários.
        User::all()->each(function(User $user){
            factory(Employee::class, 3)->create([
                'manager_id' => $user->id,
            ]);
        });
    }
}
