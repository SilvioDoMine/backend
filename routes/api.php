<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->name('api.')->group(function() {
    # Rotas de Autenticação
    Route::post('/auth/login', 'AuthController@login')->name('login');
    Route::post('/logout', 'AuthController@logout')->name('logout');

    Route::prefix('employees')->middleware('apiValidate')->group(function() {
        Route::get('/', 'EmployeeController@index');
        Route::get('/{uuid}', 'EmployeeController@show');
        Route::post('/csv', 'EmployeeController@upload');
        Route::delete('/{employee}', 'EmployeeController@destroy');
    });
});
